#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>

#define MAX 1024  /* Tamanho maximo do buffer */
#define PORT 8080 /* porta a ser escutada */

/* Funcao que envia um arquivo txt ao servidor */
void sendFile(FILE *file, int socket, struct sockaddr_in addr)
{
    int num_bytes;

    /* Faz o envio de um datagrama com a palavra INIT para informar que um arquivo sera enviado */
    char buffer[MAX] = "INIT";

    num_bytes = strlen(buffer);
    buffer[num_bytes++] = '\0';
    sendto(socket, buffer, MAX, 0, (struct sockaddr *)&addr, sizeof(addr)); /* Envia os dados para o servidor */

    /* Enquanto não se chega ao fim do arquivo, os dados do mesmo
    continuam sendo enviados */
    while (fgets(buffer, MAX, file) != NULL)
    {
        sendto(socket, buffer, MAX, 0, (struct sockaddr *)&addr, sizeof(addr));

        bzero(buffer, MAX); /* Funcao que zera o buffer */
    }

    /* Envia END para informar que o arquivo chegou ao fim */
    strcpy(buffer, "END");
    sendto(socket, buffer, MAX, 0, (struct sockaddr *)&addr, sizeof(addr));

    bzero(buffer, MAX);

    /* fecha o arquivo */
    fclose(file);

    printf("\nArquivo enviado com sucesso!\n");

    return;
}

/* Funcao que envia uma mensagem ao servidor */
void sendMessage(int socket, struct sockaddr_in addr)
{
    char buffer[MAX], aux[MAX];
    int num_bytes;

    aux[0]='\0';
    printf("\nDigite a mensagem a ser enviada: ");
    scanf(" %1024[^\n]s", buffer);

    strcat(aux, "1");
    strcat(aux, buffer);

    num_bytes = strlen(aux);
    aux[num_bytes++] = '\0'; /* Insere o \0 para indicar o termino da string */

    sendto(socket, aux, MAX, 0, (struct sockaddr *)&addr, sizeof(addr));

    printf("Mensagem enviada!\n");

    bzero(buffer, MAX);
    bzero(aux, MAX);

    return;
}

/* Funcao que encerra o servidor */
void closeConnection(int socket, struct sockaddr_in addr)
{
    sendto(socket, "3", MAX, 0, (struct sockaddr *)&addr, sizeof(addr));
    return;
}

int main(void)
{
    char *ip = "127.0.0.1"; /* IP Localhost */
    int server_socket, running = 1;
    struct sockaddr_in server_address; /* informacoes do servidor e do cliente */
    char op, *ext;
    FILE *file;
    char filepath[MAX];

    /* PF_INET = Formato de pacote, Internet = IP, TCP / IP ou UDP / IP */
    server_socket = socket(PF_INET, SOCK_DGRAM, 0);

    server_address.sin_family = AF_INET;            /* AF_INET: as comunicacoes sao feitas atraves de enderecos do Protocolo da Internet v4 */
    server_address.sin_port = htons(PORT);          /* Aqui é onde se informa a porta a ser utilizada */
    server_address.sin_addr.s_addr = inet_addr(ip); /* Aqui é onde se informa o endereco IP a ser utilizado */

    while (running)
    {
        /* Opcao 1 envia uma mensagem
        Opcao 2 envia um arquivos
        Opcao 3 fecha a conexao e encerra o servidor */
        printf("\nEscolha uma opcao:\n1- Enviar uma mensagem\n2- Enviar um arquivo\n3- Sair\n\n");
        scanf(" %c", &op);

        if (op == '1')
        {
            sendMessage(server_socket, server_address);
        }
        else if (op == '2')
        {
            printf("\nInsira o caminho do arquivo: ");
            scanf(" %[^\n]s", filepath);

            ext = strrchr(filepath, '.');

            if (!ext || (!strcmp(ext + 1, "txt") == 0)) /* Verifica se há extensao no  arquivo*/
            {
                printf("\nApenas arquivos txt são aceitos\n");
            }
            else
            {
                file = fopen(filepath, "r"); /* caso o arquivo nao exista, ele eh criado */

                if (file == NULL)
                {
                    printf("\nArquivo não encontrado.\n");
                }
                else
                {
                    sendFile(file, server_socket, server_address); /* chama a funcao que envia o arquivo */
                }
            }
        }
        else if (op == '3')
        {
            closeConnection(server_socket, server_address); /* Chama a funcao que fecha o servidor */
            running = 0;
        }
        else
        {
            printf("\nOpção inválida\n"); /* Caso o usuário digite alguma opcao invalida do menu */
        }
    }

    printf("\n\nEncerrando a conexão...\n\n");
    close(server_socket); /* Encerra a conexao socket */

    return 0;
}