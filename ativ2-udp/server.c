#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>

#define MAX 1024  /* tamanho do buffer */
#define PORT 8080 /* porta a ser escutada */

/* Funcao que executa as funcoes de escrita de um novo arquivo, printa a mensagem recebida
e encerra a conexao caso o usuário envie a opcao 3 */
void handleClient(int socket, struct sockaddr_in addr)
{
    int running = 1, fileIsOpen = 0;
    FILE *file;
    char *filename = "server.txt";
    char buffer[MAX];
    socklen_t addr_size;

    while (running)
    {
        bzero(buffer, MAX);
        addr_size = sizeof(addr);
        recvfrom(socket, buffer, MAX, 0, (struct sockaddr *)&addr, &addr_size);
      
        if (strncmp("1", buffer, strlen("1")) == 0) /* Printa a mensagem recebida */
        {
            memmove(buffer, buffer + 1, strlen(buffer));
            printf("\nMensagem recebida: %s\n", buffer);
        }
        else if (strncmp("3", buffer, strlen("3")) == 0) /* Encerra a conexao */
        {
            printf("\n\nConexão encerrada\n\n");
            running = 0;
            continue;
        }
        else if (strncmp("INIT", buffer, strlen("INIT")) == 0) /* Caso receba INIT, logo chegarao os dados de um arquivo */
        {
            file = fopen(filename, "w");
            fileIsOpen = 1;
        }
        else if (strcmp(buffer, "END") == 0) /* Ao receber END, significa que os datagramas se encerraram */
        {
            fileIsOpen = 0;
            fclose(file);
            printf("\nArquivo recebido!\n");
            continue;
        }
        /* Caso nao seja INIT ou END e fileIsOpen seja verdadeiro, o buffer eh gravado no arquivo server.txt */
        if (fileIsOpen && strcmp(buffer, "INIT") != 0 && strcmp(buffer, "END") != 0)
        {
            fprintf(file, "%s", buffer);
        }
    }

    return;
}

/* Funcao que printa na tela a mensagem recebida do cliente */
void printMessage(int socket, struct sockaddr_in addr)
{
    socklen_t addr_size;
    char buffer[MAX];

    addr_size = sizeof(addr);
    recvfrom(socket, buffer, MAX, 0, (struct sockaddr *)&addr, &addr_size);
    printf("\nMensagem recebida: %s\n", buffer);

    bzero(buffer, MAX);

    return;
}

int main(void)
{
    char *ip = "127.0.0.1";
    int server_socket;
    struct sockaddr_in server_address, client_address;

    server_socket = socket(PF_INET, SOCK_DGRAM, 0);
    if (server_socket < 0)
    {
        printf("\nErro na conexão.\n");
    }

     server_address.sin_family = AF_INET;  /* AF_INET: as comunicacoes sao feitas atraves de enderecos do Protocolo da Internet v4 */
    server_address.sin_port = htons(PORT); /* Aqui é onde se informa a porta a ser utilizada */
    server_address.sin_addr.s_addr = inet_addr(ip);  /* Aqui é onde se informa o endereco IP a ser utilizado */

    /* Relaciona server_socket com o endereco que foi definido*/
    bind(server_socket, (struct sockaddr *)&server_address, sizeof(server_address));

    printf("\nServidor Iniciado com sucesso!\n");

    handleClient(server_socket, client_address); /* Chama a funcao que efetua as funcionalidades principais */

    return 0;
}